const { defineConfig } = require('cypress');

// define what we will use as username
const prefix = 'user_';
const randomString = Math.random().toString(36).substring(2, 8);
const username = `${prefix}${randomString}`;
const password = 'toto';

module.exports = defineConfig({
  env: {
    baseUrl: process.env.CYPRESS_BASE_URL || 'http://localhost:3001/',
    username: username,
    password: password,
  },
  e2e: {
    setupNodeEvents(on, config) {
    }
  }
});
