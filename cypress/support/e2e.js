
// helpers to register and logout an user
Cypress.Commands.add('register', (username, password) => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('.fa-user').click();
    cy.get('#signUpUsername').type(username);
    cy.get('#signUpPassword').type(password);
    cy.get('#register').click();
  });
  
  Cypress.Commands.add('login', (username, password) => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('.fa-user').click();
    cy.get('#signInUsername').type(username);
    cy.get('#signInPassword').type(password);
    cy.get('#connection').click();
  });