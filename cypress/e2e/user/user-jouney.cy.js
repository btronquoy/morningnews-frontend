/// <reference types="cypress" />

// USER JOURNEY
// Test the full journey of an user - register - signin - logout - add bookmarks - delete bookmarks


// function to randomize a number 
function randomNumber() {
    return Math.floor(Math.random() * 7) + 3;
}

describe('MorningNews - User Journey', () => {

    // register
    it('Register a new user', () => {
        cy.visit(Cypress.env('baseUrl'));
        cy.get('.fa-user').click()
        cy.get('#signUpUsername').type(Cypress.env('username'))
        cy.get('#signUpPassword').type(Cypress.env('password'))
        cy.get('#register').click()
    })

    // login and signout
    it('Login and Sign out', () => {
        cy.login(Cypress.env('username'), Cypress.env('password'));
        cy.get('[class*="Header_logoutSection"] button').contains('Logout').click();
    })

    it('Login , Bookmark / UnBookmark some articles', () => {

        // login
        cy.login(Cypress.env('username'), Cypress.env('password'));

        // bookmark some articles
        cy.get('[class*="Article_articles__"]').then(($articles) => {
            // to avoid issues with the api we check that we have at least 3 available articles as we will need to bookmark min 3 articles
            expect($articles).to.have.length.at.least(3);

            const numberOfArticlesToBookmark = randomNumber()
            const bookmarkedArticles = [];

            for (let i = 0; i < numberOfArticlesToBookmark; i++) {
                const randomIndex = Math.floor(Math.random() * $articles.length);
                const randomArticle = $articles.eq(randomIndex);

                // get the article title to serve as an unique identifier
                const articleTitle = randomArticle.find('h3').text().trim();
                bookmarkedArticles.push(articleTitle);

                // bookmark the article
                cy.wrap(randomArticle)
                    .find('.fa-bookmark')
                    .click();

                // remove it from the list
                $articles.splice(randomIndex, 1);
            }

            // check the bookmark page
            cy.contains('Bookmark').click();

            // every bookmarked article should be displayed in the bookmark page
            bookmarkedArticles.forEach((title) => {
                cy.contains(title).should('be.visible');
            });
            // remove articles from bookmark
            bookmarkedArticles.forEach((title) => {
                cy.contains(title).parents('[class*="Article_articles__"]').find('.fa-bookmark').click();
            });

            // we should now have a beautifull empty bookmark page ^^
            bookmarkedArticles.forEach((title) => {
                cy.contains(title).should('not.exist');
            });

        });
    })
})
