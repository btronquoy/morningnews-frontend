# Morning App Front

Morning App Front is the front-end application of the MorningNews app. 

This project uses Jest for unit testing.

## Prerequisites

Make sure you have the following installed on your machine:

- Node.js (version 12 or higher)
- npm (version 6 or higher) or yarn

## Installation

1. Clone the repository:

    ```bash
    git clone ...
    cd your-repository
    ```

2. Install dependencies:

    ```bash
    npm install
    # or
    yarn install
    ```

## Configuration

Copy the `.env.example` file to `.env` and configure the necessary environment variables.

    ```bash
    cp .env.example .env
    ```

### Environment Variables

- `NEXT_PUBLIC_API_URL`: URL of the backend API.

Make sure to fill these variables in the `.env` file with the appropriate values.

## Running the Application

To start the application in development mode, run:

    ```bash
    npm run dev
    # or
    yarn dev
    ```

The application will be available at [http://localhost:3000](http://localhost:3000).

To build the application for production, run:

    ```bash
    npm run build
    # or
    yarn build
    ```

To start the application in production mode, run:

    ```bash
    npm run start
    # or
    yarn start
    ```

## Testing

Unit tests are written with Jest. To run the tests, use:

    ```bash
    npm run test
    # or
    yarn test
    ```

To run the tests in watch mode, use:

    ```bash
    npm run test:watch
    # or
    yarn test:watch
    ```

## Continuous Integration

This project uses GitLab CI for continuous integration. The CI/CD pipeline configuration is located in the `.gitlab-ci.yml` file.

### CI Stages

1. **Test**: Run unit tests with Jest.
2. **Trigger-Central**: Capture and trigger a central pipeline which is private.
3. **Wait-for-Validation**: Wait for manual validation from the QA team.

## Contribution

Contributions are not possible but you can open a ticket if needed